# Darty Client Review Analysis

This PoC will present the ability of webscrappying, text mining (use of python), ...

**Procedure**

- Getting data from [Darty](http://www.darty.com/)
- Use R's XML package to parse the HTML file and get the appropriate data
- Create a Corpus and performe analysis over the corpus
- Create a shiny application to present the result

## Getting data from darty & Create data frame

See code in `01scrapping`, all the scrapping functionalities come from `XML` package of R.

### Available DataSet

Available Data:

 | File Name                              | Content         |
 | -------------------------------------- | :-------------: |
 | 01scrapping/data/product_href.rds      | Product Name (id), URL (product, reviews)  |
 | 01scrapping/data/df_product_info.rds   | Prodcut Page Information        |
 | 01scrapping/data/df_product_review.rds | Review Information        |

**id -> Product Name**

### Code Book

#### df_product_info

- id
- URL
- product_family
- brand *
- price * 
- description
- note_mean * 
- ratio_q_p * 
- index_easy*  
- Character Table with detailed descriptions

#### df_product_review

- id
- page
- note_mean * 
- review_date * 
- name
- Nv_M_produit * 
- codePostal * 
- sex * 
- age * 
- ratio_q_p * 
- index_easy * 
- review_pros
- review_cons
- review_title
- review_content

## Shiny Application Creation

### Modification of rCharts 

For getting the support of Heatmap, a modification is necessary in rCharts library.

We have to download the files from [http://www.highcharts.com/download] and import in `rCharts` library of `libraries\highcharts\js` with new version and a file `heatmap.js`.

We should modify as well in the file `libraries\highcharts\config.yml`:

```yml
highcharts:
  jshead:
    - js/jquery-1.9.1.min.js
    - js/highcharts.js
    - js/highcharts-more.js
    - js/exporting.js
    - js/highcharts
  cdn:
    jshead:
      - "http://code.jquery.com/jquery-1.9.1.min.js"
      - "http://code.highcharts.com/highcharts.js"
      - "http://code.highcharts.com/highcharts-more.js"
      - "http://code.highcharts.com/modules/exporting.js"
      - "http://code.highcharts.com/modules/heatmap.js"
```

