geocoder <-
function(addr, proxy = T, key = "Aq1OdgJVGKWyPuRcaIpgsG8pZcSzy0wLF31OVyY8sSq0sWA5npVn-MAAU9sVU97f") {
require(httr)
require(RJSONIO)
reset_config()
if(proxy == T) set_config(use_proxy(url="defrceprx01.ey.net", port=8080))
link <- "http://dev.virtualearth.net/REST/v1/Locations?q="
q_key <- paste("&key=", key, sep = "")
add_returned <- try(GET(paste(link,addr,q_key,sep="")))
json <- try(fromJSON(content(add_returned, as ="text")))
if (class(json) != "try-error") {
if (json$statusCode == 200) {
if (json$resourceSets[[1]]$estimatedTotal > 0) {
coordinates <- data.frame(lat = json$resourceSets[[1]]$resources[[1]]$geocodePoints[[1]]$coordinates[1], lon = json$resourceSets[[1]]$resources[[1]]$geocodePoints[[1]]$coordinates[2])
add_detailed <- json$resourceSets[[1]]$resources[[1]]$address
add_detailed <- data.frame(t(json$resourceSets[[1]]$resources[[1]]$address))
data <- cbind(addr, coordinates,add_detailed)
rownames(data) <- NULL
}
else data <- data.frame(addr)
}
else {
data <- data.frame(addr)
}
}
else data <- data.frame(addr)
rownames(data) <- NULL
return(data)
}
