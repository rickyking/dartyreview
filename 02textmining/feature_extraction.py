import nltk
import time #import the time library; used for determining how quickly the code completed
import multiprocessing
import pandas as pd
from nltk.tag.stanford import POSTagger
pos_tagger = nltk.tag.stanford.POSTagger("/home/yjin/darty/stanford-postagger-full-2014-06-16/models/french.tagger","/home/yjin/darty/stanford-postagger-full-2014-06-16/stanford-postagger.jar",encoding='UTF-8')

def chunker(tokens, chunks):
    '''this function -- generator -- chunks a set of tokens based on a number of chunks;
    each "chunk" of tokens will be sent to a separate process;
    internal use
    '''
    chunk_size = len(tokens) // chunks #the size of each chunk (num of tokens // chunk)
    chunk_size_module = len(tokens) % chunks
    print "Total Documents: %s" % len(tokens)
    for x in xrange(0, len(tokens), chunk_size + chunk_size_module): #go through the original tokens in an increment equal to the size of each chunk
        yield tokens[x:x+chunk_size + chunk_size_module] #and return subsets equal to the size of the chunks

def worker(documents):
	"""
	A multi-processing worker function, internal use
	"""
	# doc_sent = [] # a list of all documents list of all sentences : 
	nb_sent = []
	sent_tags = []
	for document in documents: 
		sents = nltk.sent_tokenize(document)
		# doc_sent.append(sent)
		#sent_words = [] # a list of all sentences list of all words
		for sent in sents: 
			sent_words = (nltk.word_tokenize(sent))
			sent_tags.append(pos_tagger.tag(sent_words))
		nb_sent.append(len(sent_tags))
	
	documents_sent = pd.DataFrame(nb_sent, index = documents.index)
	sent_tags_S = pd.Series(sent_tags)
	index = []
	for i in xrange(len(documents_sent)): 
		if i == 0:
			for j in xrange(documents_sent.iloc[i][0]): index.append(documents_sent.index[i])
		else:
			for j in xrange(documents_sent.iloc[i-1][0], documents_sent.iloc[i][0]): index.append(documents_sent.index[i])
	sent_tags_S.index = index
	print "one process done!"
	return sent_tags_S


def feature_extraction(document_list, core = 2, tagged_return = False, result_return = False, grammar=r"""
		FE: {<ADJ.*><NC.*><NC.*>?}
			{<ADJ.*><NP.*><NP.*>?}
			{<ADV.*><ADJ.*><NC.*>?}
			{<ADV.*><ADJ.*><NP.*>?}
			{<ADV.*><P.*><NC.*>?}
			{<ADV.*><ADV.*>}
			{<ADV.*><ADV.*><JJ.*>}
			{<V.*><ADJ.*>}
			{<V.*><ADV.*>}
			{<ADJ.*><P.*><NC.*>}
	"""):
	"""
	@input: 
		document_list: list (or pandas.series) of texts [documents]
		core: number of cores for concurrent computing
		tagged_return: flags for returning a list of tagged words
		result_return: flags for returning a list of trees which is grammar processed tagged words
		grammar: a regular expression string for grammar parsing
	@output: 
		feature_extraction: list of feautre_extracted
		[optional] tagged_text: a list of tagged words
		[optional] result: a list of trees which is grammar processed tagged words
	"""
	partitioned_text = list(chunker(document_list, core)) #partition the tokens up into X number of chunks [it's a nested list]
	p = multiprocessing.Pool(core) #create a pool of processes equal to the number of chunks
	start_time=time.time() #begin timer
	tagged_text_indexed = p.map(func=worker,iterable=partitioned_text) #using map, send the partitioned_chunk nested list to the processes in the pool
	# p.close() #close the pool of processes
	p.close()
	p.join()
	tagged_text_indexed = pd.concat(tagged_text_indexed) #now reduce the returned chunks (now uppercase) into a single list again
	end_time=time.time() #end the timer
	print "%s Core(s) - preprocessing time: %s second(s)" %(core, end_time-start_time)
	cp = nltk.RegexpParser(grammar)
	result = []
	for obs in tagged_text_indexed: result.append(cp.parse(obs))
	result = pd.Series(result, index = tagged_text_indexed.index)
	# how to presente ? a list of available potential feature
	feature_extraction = []
	feature_extraction_index = []
	for i in xrange(len(result)):
		tree = result.iloc[i]
		for subtree in tree.subtrees():
			if subtree.node =="FE":  
				feature_extraction.append(subtree)
				feature_extraction_index.append(result.index[i])
	feature_extraction = pd.Series(feature_extraction, index=feature_extraction_index)
	if tagged_return == False and result_return == False:
		return feature_extraction
	elif tagged_return == True and result_return == False:
		return feature_extraction, tagged_text_indexed
	elif tagged_return == False and result_return == True:
		return feature_extraction, result
	else:
		return feature_extraction, tagged_text_indexed, result


def grammar_extraction(tagged_text,grammar=r"""
		FE: {<ADJ.*><NC.*><NC.*>?}
			{<ADJ.*><NP.*><NP.*>?}
			{<ADV.*><ADJ.*><NC.*>?}
			{<ADV.*><ADJ.*><NP.*>?}
			{<ADV.*><P.*><NC.*>?}
			{<ADV.*><ADV.*>}
			{<ADV.*><ADV.*><JJ.*>}
			{<V.*><ADJ.*>}
			{<V.*><ADV.*>}
			{<ADJ.*><P.*><NC.*>}
	""", result_return = False):
	"""
	@input:
		tagged_text: a list of tagged words
		grammar: a regular expression string for grammar parsing
	@output:
	"""
	cp = nltk.RegexpParser(grammar)
	result = []
	for obs in tagged_text_indexed: result.append(cp.parse(obs))
	result = pd.Series(result, index = tagged_text_indexed.index)
	# how to presente ? a list of available potential feature
	feature_extraction = []
	feature_extraction_index = []
	for i in xrange(len(result)):
		tree = result.iloc[i]
		for subtree in tree.subtrees():
			if subtree.node =="FE":  
				feature_extraction.append(subtree)
				feature_extraction_index.append(result.index[i])
	feature_extraction = pd.Series(feature_extraction, index=feature_extraction_index)
	if result_return == False:
		return feature_extraction
	else:
		return feature_extraction, result
