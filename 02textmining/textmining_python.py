import os
os.chdir('/home/yjin/darty/')

# import the text corpus
import pandas as pd
df = pd.read_csv("data/df_product_review.csv", sep=",")
corpus = df["review_content"]
# feature extraction with feature_extraction.py which is modified to adapt the french language
import feature_extraction as fe
tv, tagged_tv, result_tv = fe.feature_extraction(corpus, core = 16, tagged_return=True, result_return=True) # We get all three variables

tv.to_pickle("data/tv.pkl")
tagged_tv.to_pickle("data/tagged_tv.pkl")
result_tv.to_pickle("data/result_tv.pkl")

# This part of code is the continue of feature selection 
len(tv)
# 3317
df_tv = pd.DataFrame(tv, columns = ["tree"]) # result from feature extraction
leaves = []
for tree in df_tv["tree"]: leaves.append(tree.leaves()) # the extracted paterns are trees
string = []
for leave in leaves:
    dictionary = [i[0] for i in leave]
    string.append(" ".join(dictionary))
df_tv["paterns"] = string # the paterns are extracted
df_tv.to_pickle("/ccc/scratch/cont008/loreal/payem/df_mascara.pkl")


# to low, remove punctuation, stemming, lemming(not available for french)
import nltk
from nltk.stem.snowball import FrenchStemmer #import the French stemming library
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
porter = FrenchStemmer()
words_preprocessed = []
for text in df_tv["paterns"]:
    tokens = [word for sent in nltk.sent_tokenize(text) for word in tokenizer.tokenize(sent)]
    words = [w.lower() for w in tokens]
    words_stemmed = [porter.stem(t) for t in words]
    words_preprocessed.append(" ".join(words_stemmed))
df_tv["preprocessed"] = words_preprocessed # the patern preprocessed
df_tv["document_index"] = df_tv.index # the index of documents

##########
# Creation of Matrix
import pandas as pd
import numpy
import multiprocessing
import time
term_count = df_tv.groupby(["preprocessed"])["document_index"].apply(lambda x: len(x.unique()))
terms=term_count[term_count>2].index # 205 terms
df_tv_2 = df_tv[df_tv["preprocessed"].isin(terms)]
print "import of data : ok"

df_tv_2["value"] = 1
all_term = df_tv_2["preprocessed"].unique()

# Retreive the position of paterns in a list of unique paterns
ind_term = []
for term in df_tv_2["preprocessed"]:
    ind = numpy.where(all_term == term)[0][0]
    ind_term.append(ind)

df_tv_2["ind_term"] = ind_term

# 1664 paterns
# construction of matrix 

matrix_tv = pd.pivot_table(df_tv_2, values = "value", rows = ["document_index"], cols = ["preprocessed"], fill_value = 0, aggfunc = numpy.sum)

matrix_tv.to_csv("data/matrix_patern.csv", encoding = "UTF-8")